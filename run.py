# coding: utf-8

from app import app, socketio, views

# app.run(debug = True, threaded=True)
if __name__ == '__main__':
    socketio.run(app)