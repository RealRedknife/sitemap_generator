var basePaths = {
    src: 'app/static/src/',
    dest: 'app/static/public/'
};
var paths = {
    images: {
        src: basePaths.src + 'images/',
        dest: basePaths.dest + 'images/'
    },
    scripts: {
        src: basePaths.src + 'js/',
        dest: basePaths.dest + 'js/min/'
    },
    styles: {
        src: basePaths.src + 'sass/',
        dest: basePaths.dest + 'css/'
    },
    vendor: basePaths.src + 'vendor/'
};
var appFiles = {
    styles: paths.styles.src + '**/*.scss',
    scripts: paths.scripts.src + 'main.js',
    images: paths.images.src + '**/*'
};
var vendorFiles = {
    styles: '',
    scripts: [
            paths.vendor + 'jquery/dist/jquery.js',
            paths.vendor + 'jquery-form-validator/form-validator/jquery.form-validator.js',
            paths.scripts.src+'lib/socket.io.min.js',
            paths.scripts.src+'lib/sweet-alert.min.js'
            ]
};

var AUTOPREFIXER_BROWSERS = [
  'ie >= 10',
  'ie_mob >= 10',
  'ff >= 30',
  'chrome >= 34',
  'safari >= 7',
  'opera >= 23',
  'ios >= 7',
  'android >= 4.4'
];

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var gutil = require('gulp-util');
var del = require('del');
var runSequence = require('run-sequence');

// Allows gulp --dev to be run for a more verbose output
var isProduction = true;
var sassStyle = 'compressed';

if(gutil.env.dev === true) {
    sassStyle = 'expanded';
    isProduction = false;
}

var changeEvent = function(evt) {
    gutil.log('File', gutil.colors.cyan(evt.path.replace(new RegExp('/.*(?=/' + basePaths.src + ')/'), '')), 'was', gutil.colors.magenta(evt.type));
};

// Tasks..

// Compile and Automatically Prefix Stylesheets
gulp.task('styles', function () {
  // For best performance, don't add Sass partials to `gulp.src`
  return gulp.src([ appFiles.styles ])
    .pipe($.changed('styles', {extension: '.scss'}))
    .pipe($.rubySass({
        style: sassStyle,
        precision: 10
      })
      .on('error', console.error.bind(console))
    )
    .pipe($.autoprefixer(AUTOPREFIXER_BROWSERS))
    .pipe(isProduction ? $.combineMediaQueries({
        log: true
    }) : gutil.noop())
    .pipe(gulp.dest('.tmp/styles'))
    // Concatenate And Minify Styles
    .pipe($.if('*.css', $.csso()))
    .pipe(gulp.dest(paths.styles.dest))
    .pipe($.size({title: 'styles'}));
});

// Lint and concat JavaScript
gulp.task('scripts', function() {
    gulp.src(appFiles.scripts)
        .pipe($.jshint())
        .pipe($.jshint.reporter('jshint-stylish'));

    return gulp.src(vendorFiles.scripts.concat(appFiles.scripts))
        .pipe(isProduction ? $.concat('app.min.js') : $.concat('app.js'))
        .pipe(isProduction ? $.uglify() : gutil.noop())
        .pipe($.size({
            title: 'scripts'
        }))
        .pipe(gulp.dest(paths.scripts.dest));
});

// Optimize Images
gulp.task('images', function () {
  return gulp.src(appFiles.images)
    .pipe($.cache($.imagemin({
      progressive: true,
      interlaced: true
    })))
    .pipe(gulp.dest(paths.images.dest))
    .pipe($.size({title: 'images'}));
});

// Clean tmp directory
gulp.task('clean', del.bind(null, ['.tmp']));

gulp.task('watch', ['styles', 'scripts'], function(){
    gulp.watch(appFiles.styles, ['styles']).on('change', function(evt) {
        changeEvent(evt);
    });

    gulp.watch(appFiles.scripts, ['scripts']).on('change', function(evt) {
        changeEvent(evt);
    });

    gulp.watch(appFiles.images, ['images']).on('change', function(evt) {
        changeEvent(evt);
    });
});

// Build Production Files, the Default Task
gulp.task('default', ['clean'], function (cb) {
  runSequence(['styles', 'scripts', 'images', 'watch'], cb);
});
