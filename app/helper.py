# -*- coding: utf-8 -*-
from datetime import datetime
from time import strftime
from os import path

def create_xml(name, urls):
    cur_time = datetime.now().strftime("%d-%m-%Y_%H-%M-%S")
    fname = name+cur_time+".xml"
    xml_file = open(fname, "w")

    result = """<?xml version="1.0" encoding="UTF-8"?>
<urlset
      xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
<!-- created with Free Online Sitemap Generator www.sitemap.s74.org -->\n"""

    for url in urls:
        result += "<url>\n\t<loc>"+url+"</loc>\n</url>\n"

    result += "</urlset>"

    xml_file.write(result)
    xml_file.close()

    return path.abspath(fname)