# -*- coding: utf-8 -*-
from grab.spider import Spider, Task
from urlparse import urlparse, urljoin
from app import socketio
from threading import Thread

def generate_ignore_links(file_types):
    result = '['
    for ftype in file_types[:-1]:
        result += 'not(substring(@href, string-length(@href)-'+str(len(ftype)-1)+')="'+ftype+'") and '
    result += 'not(substring(@href, string-length(@href)-'+str(len(file_types[len(file_types)-1])-1)+')="'+file_types[len(file_types)-1]+'")]'
    return result

ignore_file_links = generate_ignore_links((".jpg", ".jpeg", ".png", ".pdf", ".css", ".js", ".svg", ".less", ".stylus", ".zip", ".rar", ".tar", ".tgz", ".sass", ".scss"))

class SitemapSpider(Spider):
    def __init__(self, site_url, **kwds):
        socketio.emit('start', '', namespace='/test')
        self.explored_links = set();
        self.initial_urls = [site_url]
        self.base_url = site_url
        self.hostname = urlparse(site_url).hostname
        Spider.__init__(self, **kwds)

    def prepare(self):
        self.result = []

    def task_initial(self, grab, task):
        if grab.response.code == 200:
            for elem in grab.doc.select('//a[@href != "#"]'+ignore_file_links):
                cur_url = urljoin(self.base_url, elem.attr('href'))
                if cur_url.endswith(('/', '/.')):
                    cur_url = cur_url.rstrip('//.')
                cur_url_hostname = urlparse(cur_url).hostname
                if cur_url_hostname == self.hostname and cur_url not in self.explored_links:
                    yield Task('save', url=cur_url)
                    self.explored_links.add(cur_url)

    def task_save(self, grab, task):
        if grab.response.code == 200:
            thread = Thread(target=socketio.emit('terminal',
                                                  {'url': task.url.encode('utf-8')},
                                                  namespace='/test'))
            thread.start()
            self.result.append( task.url.encode('utf-8').replace('//www.', '//', 1) )
            self.add_task(Task('initial', url=task.url.encode('utf-8')))

def main():
    return 0

if __name__ == '__main__':
    main()