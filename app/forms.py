#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask.ext.wtf import Form
from wtforms import StringField, BooleanField
from wtforms.validators import DataRequired, Email, URL

class SiteForm(Form):
    site_url = StringField('site_url', validators = [URL(message=u"Некорректный URL сайта")])
    user_email = StringField('user_email', validators=[Email(message=u"Некорректный E-mail адрес")])