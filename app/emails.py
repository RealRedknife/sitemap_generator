# -*- coding: utf-8 -*-

from flask.ext.mail import Message
from flask import render_template

from app import mail
from app import app
from config import ADMINS


def send_email(subject, sender, recipients, text_body, html_body, attachment):
    msg = Message(subject, sender = sender, recipients = recipients)
    msg.body = text_body
    msg.html = html_body
    with app.open_resource(attachment) as fp:
        msg.attach('sitemap.xml', "text/xml", fp.read())
    mail.send(msg)

def sitemap_send(site_url, user_email, attach):
    send_email("Sitemap для {0}".format(site_url),
        ADMINS[0],
        [user_email],
        render_template("sitemap_email.txt",
            url = site_url),
        render_template("sitemap_email.html",
            url = site_url),
        attach)