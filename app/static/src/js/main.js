jQuery(function() {
	(function($){

        function sendAjaxForm(result_id,form_id,url) {
            $.ajax({
                url: url,
                type: "POST",
                dataType: "html",
                data: $("#" + form_id).serialize(),
                success: function (response) {
                },
                error: function (response) {
                }
            });
        }

        // form validation init..
        var myLanguage = {
          errorTitle : 'Form submission failed!',
          requiredFields : 'Заполните поле',
          badTime : 'You have not given a correct time',
          badEmail : 'Введите корректный E-mail',
          badTelephone : 'You have not given a correct phone number',
          badSecurityAnswer : 'You have not given a correct answer to the security question',
          badDate : 'You have not given a correct date',
          lengthBadStart : 'You must give an answer between ',
          lengthBadEnd : ' символов',
          lengthTooLongStart : 'You have given an answer longer than ',
          lengthTooShortStart : 'Введите больше ',
          notConfirmed : 'Values could not be confirmed',
          badDomain : 'Incorrect domain value',
          badUrl : 'Введите корректный URL',
          badCustomVal : 'Неправильно заполнено поле',
          badInt : 'Введите номер',
          badSecurityNumber : 'Your social security number was incorrect',
          badUKVatAnswer : 'Incorrect UK VAT Number',
          badStrength : 'The password isn\'t strong enough',
          badNumberOfSelectedOptionsStart : 'You have to choose at least ',
          badNumberOfSelectedOptionsEnd : ' answers',
          badAlphaNumeric : 'The answer you gave must contain only alphanumeric characters ',
          badAlphaNumericExtra: ' and ',
          wrongFileSize : 'The file you are trying to upload is too large',
          wrongFileType : 'The file you are trying to upload is of wrong type',
          groupCheckedRangeStart : 'Please choose between ',
          groupCheckedTooFewStart : 'Please choose at least ',
          groupCheckedTooManyStart : 'Please choose a maximum of ',
          groupCheckedEnd : ' item(s)'
        };

        $.validate({
            form : '#sitemapForm',
            language : myLanguage,
            scrollToTopOnError : false,
            onSuccess : function() {
                $('#sendForm').val('').addClass('disabled');
                $('#preloader').show();
              return false; // Will stop the submission of the form
            }
          });

        namespace = '/test';

        var socket = io.connect('http://' + document.domain + ':' + location.port + namespace);

        // socket.on('connect', function() {
        //     console.log('connected');
        // });

        // socket.on('message', function(msg) {
        //     console.log(msg);
        // });

        // socket.on('disconnect', function() {
        //     console.log('disconnected');
        // });

        $btn = $('#sendForm');
        $btn.on('click', function(e){
            var $okBtn = $('.sweet-alert > .confirm');
            var $urlsWrap = $('#terminal-urls');
            resp = sendAjaxForm('wrapper', 'sitemapForm', '/');
            socket.on('start', function(msg) {
                $('#sitemapForm input[type="text"]').val('');
                swal("Заявка в обработке!", "Файл с картой сайта будет выслан вам на E-mail", "success");
                socket.on('terminal', function(msg) {
                    $urlsWrap.append('<p>Status: '+ msg.url);
                });
                socket.on('jobdone', function(msg) {
                    $urlsWrap.append('<p>Status: '+ msg.msg);
                    $okBtn.show(150);
                });
            });
        });

    })(jQuery);
});