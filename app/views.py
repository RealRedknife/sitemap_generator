# -*- coding: utf-8 -*-

from app import app, socketio
from flask import Flask, request, flash, redirect, render_template, session
from forms import SiteForm
from emails import sitemap_send
from spider import SitemapSpider
from helper import create_xml
import logging

from gevent import monkey
monkey.patch_all()

@app.route('/', methods = ['GET', 'POST'])
@app.route('/index', methods = ['GET', 'POST'])
def index():
    form = SiteForm()
    if form.validate_on_submit():
        logging.basicConfig(level=logging.ERROR)
        url = request.form['site_url']
        bot = SitemapSpider(thread_number=10, network_try_limit=1, site_url=url)
        bot.run()
        fname = create_xml('sitemap', bot.result)
        socketio.emit('terminal',
                      {'url': 'DONE!'},
                      namespace='/test')
        sitemap_send(form.site_url.data, form.user_email.data, fname)
        socketio.emit('jobdone',
                      {'msg': 'E-mail send!'},
                      namespace='/test')
        return redirect('/')
    return render_template('index.html', form = form)

@socketio.on('getsession', namespace='/test')
def msg(message):
    status = session.get('status', '0')
    socketio.emit('message',
                  {'data': status},
                  namespace='/test')


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404
