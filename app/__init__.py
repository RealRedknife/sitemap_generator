from flask import Flask
from flask.ext.mail import Mail
# from flask.ext.socketio import SocketIO
from flask_socketio import SocketIO

app = Flask(__name__)
app.config.from_object('config')
socketio = SocketIO(app)
mail = Mail(app)
